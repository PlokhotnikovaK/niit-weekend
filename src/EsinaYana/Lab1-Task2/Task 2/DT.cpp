﻿#include "DT.h"
#pragma warning(disable : 4996)
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

string WD[7] = { "Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота" };
string MonthYears[12] = { "Января","Февраля","Марта","Апреля","Мая","Июня","Июля","Августа","Сентября","Октября","Ноября","Декабря" };

time_t DateTime::setDT()
{
	struct tm *data;
	time_t sec;
	time(&sec);
	data = localtime(&sec);
	data->tm_mday = Day;
	data->tm_mon = Month;
	data->tm_year = Year - 1900;
	data->tm_hour = 1;
	data->tm_sec = 0;
	data->tm_min = 0;
	return sec = mktime(data);
}
time_t DateTime::setDTNow()
{
	struct tm *data;
	time_t sec;
	time(&sec);
	data = localtime(&sec);
	Month = data->tm_mon;
	Day = data->tm_mday;
	Year = data->tm_year+1900;
	return sec;
}

int DateTime::getToday() const
{
	struct tm *now;
	now = localtime(&sec);
	cout << "Сейчас:" << endl;
	cout << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << endl;
	cout << WD[now->tm_wday] << " " << now->tm_mday << " " << MonthYears[now->tm_mon] << " " << now->tm_year + 1900 << endl;
	return 0;
}

int DateTime::getYesterday() const
{
	struct tm *now;
	struct tm *temp;
	now = localtime(&sec);
	temp = localtime(&sec);
	now->tm_mday = temp->tm_mday - 1;
	mktime(now);
	cout << "Вчера: " << endl;
	cout << WD[now->tm_wday] << " " << now->tm_mday << " " << MonthYears[now->tm_mon] << " " << now->tm_year + 1900 << endl;
	return 0;
}
int DateTime::getTomorrow() const
{
	struct tm *now;
	struct tm *temp;
	now = localtime(&sec);
	temp = localtime(&sec);
	now->tm_mday = temp->tm_mday + 1;
	mktime(now);
	cout << "Завтра: " << endl;
	cout << WD[now->tm_wday] << " " << now->tm_mday << " " << MonthYears[now->tm_mon] << " " << now->tm_year + 1900 << endl;
	return 0;
}
int DateTime::getFuture(int nDay) const
{
	struct tm *now;
	struct tm *temp;
	now = localtime(&sec);
	temp = localtime(&sec);
	now->tm_mday = temp->tm_mday + nDay;
	mktime(now);
	cout << "Через " << nDay << " дней будет: " << endl;
	cout << WD[now->tm_wday] << " " << now->tm_mday << " " << MonthYears[now->tm_mon] << " " << now->tm_year + 1900 << endl;
	return 0;
}
int DateTime::getFuture(int nDay, int Day, int Month, int Year) const
{

	struct tm *date;
	date = localtime(&sec);
	date->tm_year = Year = Year - 1900;
	date->tm_mon = Month = Month - 1;
	date->tm_mday = Day = Day;
	mktime(date);
	cout << "Через " << nDay << " дней после " << WD[date->tm_wday] << " " << date->tm_mday << " " << MonthYears[date->tm_mon] << " " << date->tm_year + 1900 << " будет: " << endl;
	struct tm *temp;
	temp = date;
	temp->tm_mday = date->tm_mday + nDay;
	mktime(temp);
	cout << WD[temp->tm_wday] << " " << temp->tm_mday << " " << MonthYears[temp->tm_mon] << " " << temp->tm_year + 1900 << endl;
	return 0;
}
int DateTime::getPast(int nDay) const
{
	struct tm *now;
	struct tm *temp;
	now = localtime(&sec);
	temp = localtime(&sec);
	now->tm_mday = temp->tm_mday - nDay;
	mktime(now);
	cout << nDay << " дней назад было: " << endl;
	cout << WD[now->tm_wday] << " " << now->tm_mday << " " << MonthYears[now->tm_mon] << " " << now->tm_year + 1900 << endl;
	return 0;
}
int DateTime::getPast(int nDay, int Day, int Month, int Year) const
{

	struct tm *date;
	date = localtime(&sec);
	date->tm_year = Year = Year - 1900;
	date->tm_mon = Month = Month - 1;
	date->tm_mday = Day = Day;
	mktime(date);
	cout << "За " << nDay << " дней до " << WD[date->tm_wday] << " " << date->tm_mday << " " << MonthYears[date->tm_mon] << " " << date->tm_year + 1900 << " будет: " << endl;
	struct tm *temp;
	temp = date;
	temp->tm_mday = date->tm_mday - nDay;
	mktime(temp);
	cout << WD[temp->tm_wday] << " " << temp->tm_mday << " " << MonthYears[temp->tm_mon] << " " << temp->tm_year + 1900 << endl;
	return 0;
}

int DateTime::getMonth() const
{
	string MonthYears1[12] = { "Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь" };
	struct tm *now;
	now = localtime(&sec);
	cout << "Месяц: " << MonthYears1[now->tm_mon] << endl;
	return 0;
}
int DateTime::getWeekDay() const
{
	struct tm *now;
	now = localtime(&sec);
	cout << "День недели: " << WD[now->tm_wday] << endl;;
	return 0;
}
int DateTime::calcDifference(int Day1, int Month1, int Year1, int Day2, int Month2, int Year2)
{
	const int ONEDAY = 60 * 60 * 24;
	time_t d, d1, d2;
	struct tm *date1;
	time_t sec1;
	time(&sec1);
	date1 = localtime(&sec1);
	date1->tm_year = Year1 - 1900;
	date1->tm_mon = Month1 - 1;
	date1->tm_mday = Day1;
	d1 = mktime(date1);
	cout << "1ая дата: " << endl;
	cout << WD[date1->tm_wday] << " " << date1->tm_mday << " " << MonthYears[date1->tm_mon] << " " << date1->tm_year + 1900 << endl;
	struct tm *date2;
	time_t sec2;
	time(&sec2);
	date2 = localtime(&sec2);
	date2->tm_year = Year2 - 1900;
	date2->tm_mon = Month2 - 1;
	date2->tm_mday = Day2;
	d2 = mktime(date2);

	if (d2 > d1)
		d = d2 - d1;
	else if (d2 < d1)
		d = d1 - d2;
	else d = 0;

	cout << "2ая дата: " << endl;
	cout << WD[date2->tm_wday] << " " << date2->tm_mday << " " << MonthYears[date2->tm_mon] << " " << date2->tm_year + 1900 << endl;

	cout << "Разница между ними: " << d / ONEDAY << endl;

	return 0;
}

int DateTime::calcDifference(int Day1, int Month1, int Year1)
{
	const int ONEDAY = 60 * 60 * 24;
	time_t d, d1, d2;
	struct tm *date1;
	time_t sec1;
	time(&sec1);
	date1 = localtime(&sec1);
	date1->tm_year = Year1 - 1900;
	date1->tm_mon = Month1 - 1;
	date1->tm_mday = Day1;
	d1 = mktime(date1);
	cout << "1ая дата: " << endl;
	cout << WD[date1->tm_wday] << " " << date1->tm_mday << " " << MonthYears[date1->tm_mon] << " " << date1->tm_year + 1900 << endl;
	struct tm *date2;
	time_t sec;
	time(&sec);
	date2 = localtime(&sec);
	d2 = mktime(date2);

	if (d2 > d1)
		d = d2 - d1;
	else if (d2 < d1)
		d = d1 - d2;
	else d = 0;

	cout << "2ая дата-сегодня: " << endl;
	cout << WD[date2->tm_wday] << " " << date2->tm_mday << " " << MonthYears[date2->tm_mon] << " " << date2->tm_year + 1900 << endl;

	cout << "Разница между ними: " << d / ONEDAY << endl;

	return 0;
}
