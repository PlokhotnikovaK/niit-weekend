#include<vector>
#include<iterator>
#include<sstream>
#include<algorithm>
#include<ctime>
#include<cmath>
using namespace std;

class Bigest_int
{
private:
	vector<int>number;
public:
	Bigest_int(int size) 
	{
		number.reserve(size);
	}
	Bigest_int(string input) 
	{
		istringstream str(input);
		int n;
		while (str >> n) { number.push_back(n); }
	}
	int get_size()const { return number.size(); }
    const vector<int>&get_number()const{ return number;}
	int get_num(int num_dig,int pos)
	{
		int tmp = 0;
	    int result=0;
		vector<int>::iterator it;
		it = number.begin();
		if(pos>0)
		{
			while (pos)
			{
				it++;
				pos--;
			}
		}
		while (num_dig&&it != number.end())
		{
			tmp = *it;
			tmp = tmp*pow(10, num_dig - 1);
			result+=tmp;
			tmp = 0;
			num_dig--;
			it++;
		}
		return result;
	}
	string get_str()
	{
		string result;
		for (vector<int>::iterator it = number.begin(); it != number.end(); it++)
		{
			result += to_string(*it);
		}
		return result;
	}
	void show()
	{
		copy(number.begin(), number.end(), ostream_iterator<int>(cout, ""));
		cout << endl;
	}

	friend const Bigest_int operator+(const Bigest_int &n1, const Bigest_int &n2)
	{
		int max_size;
		bool flag = false;
		int count = 0;
		int diff_size = n1.get_size() - n2.get_size();
		(n1.get_size() > n2.get_size()) ? max_size = n1.get_size(), flag = true : max_size = n2.get_size();

		Bigest_int res(max_size + 1);
		Bigest_int temp1 = n1;
		Bigest_int temp2 = n2;

		if (flag)
		{
			temp2.number.resize(max_size);
			rotate(temp2.number.rbegin(), temp2.number.rbegin() + diff_size, temp2.number.rend());
		}
		else
		{
			temp1.number.resize(max_size);
			rotate(temp1.number.rbegin(), temp1.number.rbegin() - diff_size, temp1.number.rend());
		}
		vector<int>::iterator it;
		it = res.number.end();
		for (int i = max_size - 1; i >= 0; i--)
		{
			int h = temp1.number[i] + temp2.number[i];
			if (count) { h = h + 1; count--; }
			if (h >10)
			{
				count++;
				h -= 10;
				res.number.emplace(it, h);
			}
			else if (h == 10)
			{
				res.number.emplace(it, 0);
				count++;
			}
			else
			{
				res.number.emplace(it, h);
			}
			if (count&&i == 0) { res.number.emplace(it, 1); }
			h = 0;
		}
		return res;
	}
	friend const Bigest_int operator-(const Bigest_int &n1,const Bigest_int &n2)
	{
		int max_size;
		bool flag = false;
		int count = 0;
		int coun = 0;
		int h;

		(n1.get_size() > n2.get_size()) ? max_size = n1.get_size(), flag = true : max_size = n2.get_size();
		int diff_size = n1.get_size() - n2.get_size();
		Bigest_int res(max_size);
		Bigest_int temp1 = n1;
		Bigest_int temp2 = n2;

		if (flag)
		{
			temp2.number.resize(max_size);
			rotate(temp2.number.rbegin(),temp2.number.rbegin()+diff_size, temp2.number.rend());
		}
		else
		{
			temp1.number.resize(max_size);
			rotate(temp1.number.rbegin(),temp1.number.rbegin() - diff_size,temp1.number.rend());
		}
		vector<int>::iterator it;
		it = res.number.end();
			for (int i = max_size - 1; i >= 0; i--)
			{
				if (count)
				{
					h = temp1.number[i] - temp2.number[i] - 1;
					count--;
				}
				else { h = temp1.number[i] - temp2.number[i]; }
				if (h == 0)
				{
					res.number.emplace(it, h);
					coun++;
				}
				else if (h < 0)
				{
					h += 10;
					res.number.emplace(it, h);
					count++;
				}
				else { res.number.emplace(it, h); }

				h = 0;
			}
			while (coun)
			{
				if (*(res.number.begin()) == 0) { res.number.erase(res.number.begin()); }
				coun--;
			}
		return res;
	}
	friend const Bigest_int operator*(const Bigest_int&n1,const Bigest_int&n2)
	{
		/*Bigest_int temp1=n1;-------��������� ����� ��������(�����:D)
		Bigest_int temp2=n2;
		Bigest_int one("1");
		Bigest_int clone1=n1;
		Bigest_int clone2=n2;

		if (temp2<temp1)
		{
			while (temp2>1)
			{
				temp1 += clone1;
				temp2 -= one;
			}
			return temp1;
		}
		else
		{
			while (temp1>1)
			{
				temp2 += clone2;
				temp1 -= one;
			}
			return temp2;
		}*/
		Bigest_int temp1=n1;
		Bigest_int temp2=n2;
		int max_size;
		int add = 0, count = 0, rem = 0;
		bool flag = false;
		int diff_size = temp1.get_size() - temp2.get_size();

		(temp1.get_size() > temp2.get_size()) ? max_size = temp1.get_size(), flag = true : max_size = temp2.get_size();
		if (flag)
		{
			temp2.number.resize(max_size);
			rotate(temp2.number.rbegin(), temp2.number.rbegin() + diff_size, temp2.number.rend());
		}
		else
		{
			temp1.number.resize(max_size);
			rotate(temp1.number.rbegin(), temp1.number.rbegin() - diff_size, temp1.number.rend());
		}

		Bigest_int term1(max_size+1);
		Bigest_int term2(max_size+1);
		Bigest_int result(max_size*2);

		vector<int>::iterator it1, it2;
		it1= term1.number.end();
		it2 = term2.number.end();

		for (int i = max_size-1; i >= 0; i--)
		{
			for (int j = max_size-1; j >= 0; j--)
			{
				if (i == max_size - 1)
				{
					add = temp1.number[j] * temp2.number[i] + rem;
					rem = 0;
					if (add >= 10)
					{
						rem = add/10;
						add-=rem*10;
					}
				    if(j==0)
					{ 
						term1.number.emplace(it1, add); 
						if (rem)
						{
							term1.number.emplace(it1, rem);
						}
					}
					else
					{
						term1.number.emplace(it1, add);
					}
				}
				else
				{
					add = temp1.number[j] * temp2.number[i] + rem;
					rem = 0;
					if (add >= 10)
					{
						rem =add/10;
						add-=rem*10;
					}
				    if (j == 0)
					{ 
						term2.number.emplace(it2, add);
						if (rem) { term2.number.emplace(it2, rem); }
						int count_ = count;
						while (count_)
						{
							term2.number.push_back(0);
							count_--;
						}
					}
					else
					{
						term2.number.emplace(it2, add);
					}
				}
			}
			rem = 0;
			add = 0;
			result += term2;
			count++;
			term2.number.clear();
			it2 = term2.number.end();
		}
		result += term1;
		while (diff_size)
		{
			result.number.erase(result.number.begin());
			diff_size--;
		}
		return result;
	}
	friend const Bigest_int operator/(const Bigest_int&n1, const Bigest_int&n2)
	{
		int rem = 0, add = 0;
	
		int count = 1;//��������
		int cc = 0;
		int coun = 1;
		int start = 0;

		int num = 0;
		int denominator;
		int max_size;

		(n1.get_size() > n2.get_size()) ? max_size = n1.get_size() : max_size = n2.get_size();
		Bigest_int temp1 = n1;
		Bigest_int temp2 = n2;
		Bigest_int result(max_size);
		Bigest_int zero("0");
		Bigest_int one("1");
		string res;

		if (temp2.get_size() < 10)//���� �������� ���������
		{
			denominator = temp2.get_num(temp2.get_size(), 0);
			if (temp1 > temp2)
			{
				while ((start + count - 1) < temp1.get_size())
				{
					num = temp1.get_num(count, start);
					if (rem)
					{
						if (num == 0)
						{
							start++;
							num = rem * pow(10, coun);
							coun++;
						}
						else
						{
							num = num + rem *pow(10, count);
							rem = 0;
						}
					}
					if (num >= denominator)
					{
						if (coun == 1) { start += count; }
						add = num / denominator;
						result.number.push_back(add);
						rem = num - (add*denominator);
						count = 1;
						coun = 1;
						num = 0;
					}
					else if (start)
					{
						result.number.push_back(0);
						count++;
					}
					else
					{
						count++;
					}
				}
			}
			else if (temp1 == temp2) { return one; }
			else { return zero; }
			return result;
		}
		else
		{
			while (temp1 >= temp2)
			{
				temp1 -= temp2;
				cc++;
			}
			res = to_string(cc);
			int res_size = res.length();
			int con = 1;
			for (int i = 0; i < res_size; i++)
			{
				res.insert(res.begin() + con,' ');
				con += 2;
			}
			Bigest_int r(res);
			return r;
		}
	}
	friend const Bigest_int operator%(const Bigest_int&n1, const Bigest_int&n2)
	{
		Bigest_int tmp = n1 / n2;
		return n1 - n2*tmp;
	}
	friend const Bigest_int& operator*=(Bigest_int&left, const Bigest_int&right)
	{
		left = left*right;
		return left;
	}
	Bigest_int& operator=(const Bigest_int&right)
	{
		if (this == &right)
		{
			return*this;
		}
		number = right.get_number();
		return *this;
	}
	friend Bigest_int& operator+=(Bigest_int&left,const Bigest_int&right)
	{
		left=left + right;
		return left;
	}
	friend Bigest_int& operator-=(Bigest_int&left,const Bigest_int&right)
	{
		left = left-right;
		return left;
	}
	friend const bool operator<(const Bigest_int&left,const Bigest_int&right)
	{
		bool flag;
		if (left.get_size() < right.get_size())
			return true;
		else if (left.get_size() == right.get_size())
		{
			for (int i =0; i<left.get_size();i++)
			{
				if (left.get_number()[i] < right.get_number()[i]) { return true; }
				flag = false;
			}
		}
		else
			return false;

		return flag;
	}
	friend const bool operator<=(const Bigest_int&left, const Bigest_int&right)
	{
		if (left < right || left == right) { return true;}
		return false;
	}
	friend const bool operator>(const Bigest_int&left, const Bigest_int&right)
	{
		bool flag;
		if (left.get_size()>right.get_size())
			return true;
		else if (left.get_size() == right.get_size())
		{
			for (int i = 0; i<left.get_size(); i++)
			{
				if (left.get_number()[i]>right.get_number()[i]) { return true; }
				flag = false;
			}
		}
		else
			return false;

		return flag;
	}
	friend const bool operator>=(const Bigest_int&left, const Bigest_int&right)
	{
		bool flag;
		if (left.get_size()>right.get_size())
			return true;
		else if (left.get_size() == right.get_size())
		{
			for (int i = 0; i<left.get_size(); i++)
			{
				if (left.get_number()[i]>=right.get_number()[i]) { return true; }
				flag = false;
			}
		}
		else
			return false;

		return flag;
	}
	friend const bool operator>(const Bigest_int&left, int right)
	{
		string r = to_string(right);
		Bigest_int temp(r);
		bool flag;
		if (left.get_size()>temp.get_size())
			return true;
		else if (left.get_size() ==temp.get_size())
		{
			for (int i = 0; i<left.get_size(); i++)
			{

				if (left.get_number()[i]>temp.get_number()[i]) { return true; }
				flag = false;
			}
		}
		else
			return false;

		return flag;
	}
	friend const bool operator==(const Bigest_int&left,const Bigest_int&right)
	{
		int count = 0;
		if (left.get_size() < right.get_size()) { return false; }
		else if (left.get_size() == right.get_size())
		{
			for (int i = 0; i <left.get_size(); i++)
			{
				if (left.number[i] == right.number[i]) { count++; }
			}
			if (count == left.get_size())
			{ 
				return true;
			}
		}
		else{return false;}
		return false;
	}
	friend const bool operator!=(const Bigest_int&left,const Bigest_int&right)
	{
		return !(left == right);
	}
	friend const Bigest_int operator^(const Bigest_int&n1,int st)
	{
		Bigest_int temp1 = n1;
		while (st-1)
		{
			temp1*=n1;
			st--;
		}
		return temp1;
	}

};



