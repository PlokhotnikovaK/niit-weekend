#ifndef _Header_H_ 
#define _Header_H
#define _CRT_SECURE_NO_WARNINGS
#include<ctime>
class DateTime
{
private:
	time_t sec;
public:
	DateTime(unsigned int n){sec = n;}
	DateTime(){time(&sec);}
	DateTime(const DateTime&copy){}
	~DateTime() {};
	time_t get_sec()const {return sec; }
	void print_today();
	void print_yesterday();
	void print_tomorrow();
	void print_future(int);
	void print_past(int);
	unsigned int calc_diff(const DateTime&,const DateTime&);
};


#endif
