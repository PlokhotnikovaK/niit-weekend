#define _CRT_SECURE_NO_WARNINGS
#include"Header.h"
#include<iostream>
#include<string>
#include<time.h>
#include<cmath>
using namespace std;

string week[7] = { "Sunday","Monday","Tuesday" ,"Wednsday" ,"Thursday" ,"Friday","Saturday" };
string months[12] = { "January"," February","March","April","May","June","July","August","September","October",
"November","December" };

void DateTime::print_today()
{
	struct tm*temp;
	temp = localtime(&sec);
	cout << asctime(temp);
}
void DateTime::print_yesterday()
{
	cout << "Yesterday date: ";
	time_t sec_;
	sec_ = sec;
	struct tm*temp;
	sec_ = sec_-(24 * 60 * 60);
	temp = localtime(&sec_);
	cout << months[temp->tm_mon] << " " << week[temp->tm_wday] << endl;
}
void DateTime::print_tomorrow()
{
	cout << "Tomorrow date: ";
	time_t sec_;
	sec_ = sec;
	struct tm*temp;
	sec_ = sec_ + (24*60*60);
	temp = localtime(&sec_);
	cout << months[temp->tm_mon] << " " << week[temp->tm_wday] << endl;
}
void DateTime::print_future(int n)
{
	time_t sec_;
	sec_ = sec;
	struct tm*temp;
	sec_ = sec_ + (n * 24 * 60 * 60);
	temp = localtime(&sec_);
	cout << asctime(temp);
}
void DateTime::print_past(int n)
{
	time_t sec_;
	sec_ = sec;
	struct tm*temp;
	sec_ = sec_-(n * 24 * 60 * 60);
	temp = localtime(&sec_);
	cout << asctime(temp);
}
unsigned int DateTime::calc_diff(const DateTime&fir,const DateTime&sec)
{
	unsigned int temp = abs(fir.get_sec() - sec.get_sec());
	unsigned int day = temp / 3600 / 24;
	return day;
}

	

