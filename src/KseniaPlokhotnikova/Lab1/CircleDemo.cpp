


#include "CircleDemo.h"


CircleDemo::CircleDemo() :
	_radius(.0), _area(.0), _ference(.0)
{  }

void CircleDemo::setRadius(double radius)
{
	_radius = radius;
	_ference = 2 * PI * radius;
	_area = PI * radius * radius;
}

void CircleDemo::setFerence(double ference)
{
	_ference = ference;
	_radius = ference / (2 * PI);
	_area = ference * ference / (4 * PI);
}

void CircleDemo::setArea(double area)
{
	_area = area;
	_radius = sqrt(area / PI);
	_ference = 2 * 3.14 * sqrt(area / PI);
}



