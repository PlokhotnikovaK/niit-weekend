#include "DDate.h"
#include <cstdio>
#include <algorithm>

int main()
{
	DDate::printToday();
	DDate::printCurrentWeekDay();
	DDate::printTomorrow();

	DDate::printYesterday();
	DDate::printPast(5);
	
	time_t date = time(NULL);
	tm *first_timestamp = localtime(&date);
	tm second_timestamp = *first_timestamp;

	if (!first_timestamp)
		return - 1;

	//< only for test purpose, that's why I didn't conditer the month and the number of days per different months
	int day = (first_timestamp->tm_mday + 5) % 30;
	int month = (day < 5) ?
		(first_timestamp->tm_mon + 1) : (first_timestamp->tm_mon);
	
	int year = first_timestamp->tm_year;
	if (month == 12)
	{
		month--;
		year++;
	}

	second_timestamp.tm_mday = day;
	second_timestamp.tm_mon = month;
	second_timestamp.tm_year = year;

	printf("Difference in days: %d\n",
		DDate::calculateDiff(*first_timestamp, second_timestamp));
}