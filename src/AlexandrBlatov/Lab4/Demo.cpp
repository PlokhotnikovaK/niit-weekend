#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <iterator>
#include <string>
#include <Windows.h>
#include <map>
#include "Dekanat.h"

using namespace std;



int main()
{
	SetConsoleCP(1251);// ��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); // ��������� ������� �������� win-cp 1251 � ����� ������

	cout << "���� ������� ����� �� ������������, \n�������� � ��������� ������� ����� Lucida Console \n";
	cout << "If Russian text is not displayed, \nselect in the properties console font is Lucida Console \n";
	Dekanat* dek = Dekanat::Create(); //��������� ������� (��������)
	
	map<int, string> menu = { {0, "�������� ��������: \n" },
	{ 1, "1. �������� ������ ������� �����.\n" },
	{ 2, "2. ��������� ������� ������� � �������.\n" },
	{ 3, "3. ���������� �� ������������ ��������� � �����. \n" },
	{ 4, "4. ������ �� ����������.\n" },
	{ 5, "5. ������� �������� �� ������ � ������.\n" },
	{ 6, "6. ������ ��������� �� ����.\n" },
	{ 98, "������� ����� ������ ����, ��� /End/ ��� ���������� ������\n" },

	{ 30, "3. ���������� �� ������������ ��������� � �����. \n" },
	{ 31, "3.1. ������� ������ �� �������� \n" },
	{ 32, "3.2. ������� ������ �� ������. \n" },

	{ 40, "4. ������ �� ����������.\n" },
	{ 41, "4.1. ������ ��������� � ������ \n" },
	{ 42, "4.2. �������� ������ ��������. \n" },
	{ 43, "4.3. �������� ������ ��������. \n" },
	{ 44, "4.4. ���������� ��������� �� ��������������. \n" },
	{ 45, "4.5. ������ ����������� ���������.\n" },

	{ 99, "����������� � ���������� ���� /back/.\n" } };

	
	
	string str;
	int key;
	system("pause");
	cout << "����������� ������ � ������� � ���������." << endl;
	dek->setFileRead();
	int lvlMenu = 0, famMenu = 0;
	do
	{
		system("cls");

		int index = 0;
		index +=  lvlMenu;

		cout << menu[famMenu];
		for (auto it = menu.begin(); it != menu.end(); ++it)
		{
			if ((*it).first > lvlMenu && (*it).first < (lvlMenu+10))
				cout << (*it).second;
		}
		if (lvlMenu == 0)
				cout << menu[98];
			else
				cout << menu[99];
		
		cin >> str;
		key = str[0];
		key += lvlMenu-48;

		switch (key)
			{
			case 1: //�������� ������ ������� �����.
				system("cls");
				cout << "�������� ������ ������� �����.\n" << endl;
				cout << dek->getConsole(LIST_NUM_GROUP) << endl;
				system("pause");
				break;
			case 2: //��������� ������� ������� � �������.
				system("cls");
				cout << "��������� ������� ������� � �������.\n" << endl;
				dek->electHeads();
				system("pause");
				break;
			case 3: //���������� �� ������������ ��������� � �����.
				lvlMenu = 10*key;
				famMenu = key;
				break;
			case 31: //������� ������ �� ��������.
				system("cls");
				cout << "������� ������ �� ��������." << endl;
				cout << dek->getStat(STAT_AV_STUDENT) << endl;
				system("pause");
				break;
			case 32: //������� ������ �� ������.
				system("cls");
				cout << "������� ������ �� ������." << endl;
				cout << dek->getStat(STAT_AV_GROUP) << endl;
				system("pause");
				break;
			case 4://��������� ���������� �� ������.������ �� ����������.
				lvlMenu = 10 * key;
				famMenu = key;
				break;
			case 41:  //������ ��������� � ������.
				system("cls");
				cout << dek->getConsole(LIST_GROUP) << endl;
				system("pause");
				break;
			case 42: //�������� ������ ��������.
				system("cls");
				cout << "�������� ������ ��������." << endl;
				cout << dek->getConsole(LIST_STUDENT) << endl;
				system("pause");
				break;
			case 43: //�������� ������ ��������.
				system("cls");
				cout << "�������� ������ ��������." << endl;
				cout << dek->getConsole(ADD_MARK_STUDENT) << endl;
				system("pause");
				break;
			case 44: //���������� ���������
				system("cls");
				cout << "���������� ���������.\n" << endl;
				dek->getConsole(STUD_EXPELLED);
				cout << "����� ���������� ���������.\n" << endl;
				system("pause");
				break;
			case 45: //������ ����������� ���������
				system("cls");
				cout << "������ ����������� ���������.\n" << endl;
				cout << dek->getConsole(LIST_EXPELLED) << endl;
				system("pause");
				break;
			case 5: //������� �������� �� ������ � ������.
				system("cls");
				cout << "������� �������� �� ������ � ������." << endl;
				cout << dek->getConsole(STUD_TRANSFER) << endl;
				system("pause");
				break;
			case 6: //������ ��������� �� ����.
				system("cls");
				dek->getFileWrite();
				system("pause");
				break;
				};

		if (str == "back")
			{
				lvlMenu = 0;
				famMenu = 0;
			}
	} while (str != "End");
	dek->getFileWrite();
	return 0;
}